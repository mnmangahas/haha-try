/*
[Section] Selecting HTML elements and accessing element properties
	- HTML elements can be selected using the different document object methods/functions
	- Using Javascript HTML DOM, the selected element contains a number of usefull properties and even attributes that can be manipulated using JS DOM
	- for input fields, '.value' is accessible to obtain information added by the user
	- methods for selecting multiple elements returns an HTML collection which can be accessed similar to how array elements are accessed: using their index
*/

// const firstName = document.getElementById('first-name');
// console.log(firstName);
/*
other examples of JS DOM selectors
	- getElementsByClassName
	- getElementsByTagName
*/
// const firstName = document.getElementsByClassName('first-name');
// console.log(firstName[0].value);

// .querySelector is more flexible method for selecting different elements by their tagName(tag), id(#) and/or className(.)
const firstName = document.querySelector('#first-name');
// console.log(firstName.value);

/*
[Section] JS Events
	- JS events are responsible for making pages interactive
	- using event listeners, JS can listen or watch out for specific events that can be used as triggers for manipulating the DOM
	SYNTAX:
		eventTarget.addEventListener("event", function(){
			codes to be executed
		})
*/
const lastName = document.querySelector('#last-name');
const fullNameText = document.querySelector('#txt-full-name');
// console.log(fullNameText.textContent);

firstName.addEventListener('keyup', function(){
	console.log(firstName.value);
	fullNameText.textContent = 'Full Name: ' + firstName.value + " " + lastName.value;
});

lastName.addEventListener('keyup', function(){
	console.log(lastName.value);
	fullNameText.textContent = 'Full Name: ' + firstName.value + " " + lastName.value;
});

const age = document.querySelector('#age');
const ageText = document.querySelector('#txt-age');
age.addEventListener('keyup', function(){
	console.log(age.value);
	ageText.innerHTML = 'Age: Your age is <b>' + age.value + "</b>";
});

const description = document.querySelector('#description');
const descriptionText = document.querySelector('#txt-description');

description.addEventListener('keyup', function(){
	console.log(description.value);
	descriptionText.textContent = 'Description: ' + description.value;
});

const subscription = document.querySelector('#subscription');
// console.log('subscription elements:');
// console.log(subscription);
// console.log(subscription.selectedIndex);
// console.log(subscription[subscription.selectedIndex]);
// console.log(subscription[subscription.selectedIndex].value);
const subscriptionText = document.querySelector('#txt-subscription');
const priceText = document.querySelector('#txt-price');

subscription.addEventListener('change', function(){
	let subscriptionValue = subscription[subscription.selectedIndex].value;
	subscriptionText.textContent = "Subscription Type: " + subscriptionValue;
	if (subscriptionValue === "basic"){
		priceText.textContent = 'Price: 500 PHP';
	} else {
		priceText.textContent = 'Price: 1000 PHP';
	}
});